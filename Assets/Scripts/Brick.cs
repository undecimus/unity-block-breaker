﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Brick : MonoBehaviour {

	public Sprite[] Sprites;
	public AudioClip BreakingSoundClip;
	public GameObject Smoke;

	private LevelManager levelManager;

	public static int NumberOfBreakables = 0;

	private int timesHit;
	private int maxHits;



	// Use this for initialization
	void Start () {
		if (this.tag == "Breakable")
			NumberOfBreakables++;


		timesHit = 0;
		levelManager = GameObject.FindObjectOfType<LevelManager> ();

		maxHits = Sprites.Length + 1;
	}

	void OnCollisionEnter2D (Collision2D collision) {
		if (this.tag == "Breakable" && collision.collider.tag == ("Ball")) {
			PlaySound ();
			HandleHits ();
		}
	}

	void PlaySound() {
		Debug.Log ("Playing Sound");
		AudioSource.PlayClipAtPoint (this.BreakingSoundClip, transform.position);

	}

	void HandleHits() {
		timesHit++;

		if (timesHit >= maxHits) {
			NumberOfBreakables--;
			EmitSmoke ();
			levelManager.BrickDestroyed();
			Destroy (gameObject);
		} else {
			LoadSprites ();
		}
	}

	//TODO: remove when finished
	void SimulateWin(){
		levelManager.LoadNextLevel ();
	}

	void LoadSprites() {
		int spriteIndex = timesHit - 1;

		if (Sprites [spriteIndex]) {
			this.GetComponent<SpriteRenderer> ().sprite = Sprites [spriteIndex];
		} else {
			Debug.LogError ("Missing Sprite on Brick @ SpriteIndex = " + spriteIndex);
		}
	}

	void EmitSmoke() {
		GameObject smoke = Instantiate<GameObject> (Smoke, transform.position, Quaternion.identity);
		var main = smoke.GetComponent<ParticleSystem> ().main;
		main.startColor = GetComponent<SpriteRenderer> ().color;


	}
		
}
