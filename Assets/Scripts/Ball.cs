﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
	

	private Paddle paddle;
	public float InitialThrust;

	public AudioClip BallSound;

	public Vector2 InitialVelocityVector;
	public float RebounceXMin;
	public float RebounceXMax;
	public float RebounceYMin;
	public float RebounceYMax;

	private Vector3 ballToPaddleVector;
	private bool hasStarted;

	// Use this for initialization
	void Start () {
		hasStarted = false;
		paddle = GameObject.FindObjectOfType<Paddle> ();
		ballToPaddleVector = this.transform.position - paddle.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (!hasStarted) {
			this.transform.position = paddle.transform.position + ballToPaddleVector;
		
			if (Input.GetMouseButtonDown (0)) {
				hasStarted = true;
				this.GetComponent<Rigidbody2D> ().velocity = InitialVelocityVector;
			}
		}
	}

	void OnCollisionEnter2D (Collision2D collision) {
		if (collision.collider.tag == "Wall" || collision.collider.tag == "Paddle") {
			PlaySound ();
		}

		if (collision.collider.tag == "Paddle" || hasStarted) {
			Vector2 currentVelocity = this.GetComponent<Rigidbody2D> ().velocity;
			Vector2 tweak = new Vector2 (Random.Range (RebounceXMin, RebounceXMax) + currentVelocity.x, Mathf.Clamp(Random.Range (RebounceYMin, RebounceYMax) + currentVelocity.y,-8f,8f));
			this.GetComponent<Rigidbody2D> ().velocity = tweak;
			print ("Current velocity:" + this.GetComponent<Rigidbody2D> ().velocity);
		}
	}

	void PlaySound() {
		if (hasStarted) {
			AudioSource.PlayClipAtPoint (this.BallSound, transform.position);
		}
	}
}
