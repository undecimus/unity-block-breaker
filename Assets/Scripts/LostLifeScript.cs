﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LostLifeScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.GetComponent<Text> ().text = LevelManager.NumberOfLives + " Lives Remaining!";
		Debug.Log ("Changed number of lives text from START");
	}

	void Awake() {
		this.GetComponent<Text> ().text = LevelManager.NumberOfLives + " Lives Remaining!";
		Debug.Log ("Changed number of lives text from AWAKE");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
