﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseCollider : MonoBehaviour {

	private LevelManager levelManager;

	void OnTriggerEnter2D (Collider2D trigger) {

		if (trigger.CompareTag ("Ball")) {
			Debug.Log ("Lose game trigger");

			levelManager.LostLife ();

		}
	}

	void OnCollisionEnter2D (Collision2D collision) {

		if (collision.collider.CompareTag ("Ball")) {
			Debug.Log ("Lose game collision");

			levelManager.LostLife ();

		}
	}

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager> ();
	}
	
		
}
