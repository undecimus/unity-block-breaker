﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

	public bool AutoPlay = false;

	private Ball ball;
	private float numBlocks;
	private float currentMousePosInBlocks;
	private float minX;
	private float maxX;

	// Use this for initialization
	void Start () {

		ball = FindObjectOfType<Ball> ();

		numBlocks = GetNumberOfBlocks ();
		minX = 1;//GetComponentInParent<PolygonCollider2D> ().points[0].x / 2;
		maxX = 15;//(numBlocks - GetComponentInParent<PolygonCollider2D> ().points[0].x / 2);

		Debug.Log ("Number of blocks = " + numBlocks);
		Debug.Log ("Minimum X position = " + minX);
		Debug.Log ("Maximum X position = " + maxX);
	}
	
	// Update is called once per frame
	void Update () {
		if (!AutoPlay) {
			MoveWithMouse ();
		} else {
			AutoFollowBall ();
		}
	}

	void MoveWithMouse() {
		currentMousePosInBlocks = Input.mousePosition.x / Screen.width * numBlocks;
		this.transform.position = new Vector3 (Mathf.Clamp(currentMousePosInBlocks,minX,maxX), transform.position.y, transform.position.z);
	}

	void AutoFollowBall () {
		this.transform.position = new Vector3 (ball.transform.position.x,transform.position.y,transform.position.z);
	}

	float GetNumberOfBlocks() {
		float pixelsPerUnit = GameObject.Find ("Background").GetComponent<SpriteRenderer> ().sprite.pixelsPerUnit;
		return (Screen.width / pixelsPerUnit);
	}


}
