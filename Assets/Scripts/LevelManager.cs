﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {

	public static int NumberOfLives = 3;
	 
	private static int currentLevelBuildIndex;
	public string[] scenes;

	void Start() {
		print ("START");
		this.scenes = ReadNames ();
		print("number of scenes in build: "+ scenes.Length);

		#if UNITY_EDITOR
		print("UNITY_EDITOR SETTINGS");
		AdjustCurrentLevelMarker(1);
		#elif
		print("UNITY_PRODUCTION SETTINGS");
		if (SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(0))
		AdjustCurrentLevelMarker (SceneManager.GetSceneByName("Level_01").buildIndex);
		#endif
	}


/* 
 * **************************************************************************************************
 * UTILITY METHODS **********************************************************************************
 * **************************************************************************************************
*/

	#if UNITY_EDITOR
	private static string[] ReadNames()
	{
		List<string> temp = new List<string>();

	#if UNITY_EDITOR
		foreach (UnityEditor.EditorBuildSettingsScene S in UnityEditor.EditorBuildSettings.scenes)
		{
			if (S.enabled)
			{
				string name = S.path.Substring(S.path.LastIndexOf('/')+1);
				name = name.Substring(0,name.Length-6);
				temp.Add(name);
			}
		}
	#endif

		return temp.ToArray();
	}

	[UnityEditor.MenuItem("CONTEXT/LevelManager/Update Scene Names")]
	private static void UpdateNames(UnityEditor.MenuCommand command)
	{
		LevelManager context = (LevelManager)command.context;
		context.scenes = ReadNames();
	}

	private void Reset()
	{
		scenes = ReadNames();
	}
	#endif


	private void ResetNumberOfBreakables(){
		Debug.Log ("Resetting number of breakables to 0");
		Brick.NumberOfBreakables = 0;
	}

	private void AdjustCurrentLevelMarker(int nextLevelBuildIndex) {
		Debug.Log ("nextLevelBuildIndex = " + nextLevelBuildIndex);
		print("number of scenes in build: "+ scenes.Length);

		#if UNITY_EDITOR
		if(scenes[nextLevelBuildIndex].Contains("Level"))
			currentLevelBuildIndex = nextLevelBuildIndex;	
		#elif
		if(SceneManager.GetSceneByBuildIndex (nextLevelBuildIndex).name.Contains("Level"))
			currentLevelBuildIndex = nextLevelBuildIndex;	
		#endif

	}

/* 
 * **************************************************************************************************
 * LEVEL LOADING METHODS ****************************************************************************
 * **************************************************************************************************
*/

	public void LoadNextLevel() {
		Debug.Log ("Loading Next Level");
		ResetNumberOfBreakables ();

		int nextLevelBuildIndex = SceneManager.GetActiveScene ().buildIndex + 1;
		SceneManager.LoadScene (nextLevelBuildIndex);

		AdjustCurrentLevelMarker (nextLevelBuildIndex);
	}

	public void LoadLevel(string name){
		Debug.Log ("New Level load: " + name);
		ResetNumberOfBreakables ();

		SceneManager.LoadScene (name);
		AdjustCurrentLevelMarker (SceneManager.GetSceneByName (name).buildIndex);
	}

	public void LoadLevel(int buildIndex) {
		Debug.Log ("New Level load: " + name);
		ResetNumberOfBreakables ();
		SceneManager.LoadScene (buildIndex);

		AdjustCurrentLevelMarker (buildIndex);
	}

	public void QuitRequest(){
		Debug.Log ("Quit requested");
		Application.Quit ();
	}

	public void RestartGame() {
		Debug.Log ("Restarting game");
		ResetNumberOfBreakables ();
		SceneManager.LoadScene ("Start Menu");		
		//AdjustCurrentLevelMarker(SceneManager.GetSceneByName ("Level_01").buildIndex);
	}

	public void RestartLevel() {
		Debug.Log ("Restarting game");
		this.LoadLevel (currentLevelBuildIndex);
	}


/* 
 * **************************************************************************************************
 * EXTERNAL MESSAGE METHODS *************************************************************************
 * **************************************************************************************************
*/
	public void BrickDestroyed() {
		if (Brick.NumberOfBreakables <= 0) {
			Debug.Log ("Number of Bricks reduced to 0, loading new level");
			ResetNumberOfBreakables ();
			this.LoadNextLevel ();
		}
	}
		
	public void LostLife() {
		NumberOfLives--;
		Debug.Log ("Remaining Lives = " + NumberOfLives);

		if (NumberOfLives <= 0) {
			LoadLevel ("Lose Screen");
		} else {
			this.LoadLevel ("Lost Life Screen");
		}
	}
}
