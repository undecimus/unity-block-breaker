﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smoke : MonoBehaviour {

	private float destroyTimer;

	// Use this for initialization
	void Start () {
		destroyTimer = this.GetComponent<ParticleSystem> ().main.duration + 1;
	}
	
	// Update is called once per frame
	void Update () {
		Destroy (gameObject, destroyTimer);
	}
}
